package com.HassanAlthaf.StockControlSystem;

import javax.swing.*;
import java.awt.*;

public class Main {

	public static void main(String[] args) {
		final JFrame frame = new JFrame("JTable Demo");
		
		String[] columns = {"Code", "Name", "Qty", "Unit Price", "Kind", "Volume", "Discount Rate", "Price", };
		
		Object[][] data = {
				{"001", "FOOD", "100", "$$$", "Rice", 10, "$$$", " 10$",},
				{"002", "Drink", "24","$$$", "Water", 24, "$$$", " 1$"},
				{"003", "Desert","20", "$$$", "Cake", 10, "$$$", " 5$"},
				{"004", "Vegetable","50", "$$$", "Fruit", 30, "$$$", " 7$"},
				{"005", "Meat", "25", "$$$", "Beef", 30, "$$$", " 15$"},
				};
				
		JTable table = new JTable(data, columns);
		JScrollPane scrollPane = new JScrollPane(table);
		table.setFillsViewportHeight(true);
		
		JLabel lblHeading =new JLabel("Stock Table");
		lblHeading.setFont(new Font("arial",Font.TRUETYPE_FONT,24));
		
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(lblHeading,BorderLayout.PAGE_START);
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1000, 500);
		frame.setVisible(true);
		}
		// TODO Auto-generated method stub

	}


